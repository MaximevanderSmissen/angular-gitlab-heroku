import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './core/app/app.component'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { NavbarComponent } from './core/navbar/navbar.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { RouterModule } from '@angular/router'
import { UsecasesListComponent } from './about/usecases/usecases-list/usecases-list.component'
import { AngularFireModule } from '@angular/fire'
import { AngularFirestoreModule } from '@angular/fire/firestore'
import { AngularFireAuthModule } from '@angular/fire/auth'
import { environment } from 'src/environments/environment'
import { UsecaseService } from './services/usecase/usecase.service'
import { UsecaseFireBaseService } from './services/usecase/usecase.firebase.service'
import { AuthService } from './services/auth/auth.service'
import { AuthFirebaseService } from './services/auth/auth-firebase.service'
import { CharacterFirebaseService } from './services/character/character-firebase.service'
import { CharacterService } from './services/character/character.service'
import { CharacterModule } from './character/character.module'
import { CreateEditComponent } from './generic-model/create-edit/create-edit.component'
import { CompareComponent } from './compare/compare.component'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { ListComponent } from './generic-model/list/list.component'
import { ListElementComponent } from './generic-model/list-element/list-element.component'
import { GenericModelService } from './services/generic-model/generic-model.service'
import { GenericModelFirebaseService } from './services/generic-model/generic-model-firebase.service'

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    UsecasesComponent,
    UsecaseComponent,
    UsecasesListComponent,
    CreateEditComponent,
    ListComponent,
    ListElementComponent,
    CompareComponent
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    CharacterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    { provide: AuthService, useClass: AuthFirebaseService },
    { provide: CharacterService, useClass: CharacterFirebaseService },
    { provide: GenericModelService, useClass: GenericModelFirebaseService },
    { provide: UsecaseService, useClass: UsecaseFireBaseService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
