import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'
import { GenericModelMockService } from '../services/generic-model/generic-model-mock.service'
import { GenericModelService } from '../services/generic-model/generic-model.service'

import { DetailComponent } from './detail.component'

describe('DetailComponent', () => {
  let component: DetailComponent
  let fixture: ComponentFixture<DetailComponent>
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DetailComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(DetailComponent, {
      set: {
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              params: of({ id: 'test' }),
              pathFromRoot: [
                {
                  /* Ignored */
                },
                {
                  url: of([{ path: 'character' }])
                }
              ]
            }
          },
          { provide: GenericModelService, useClass: GenericModelMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(DetailComponent)
    component = fixture.componentInstance
    element = fixture.nativeElement
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have the correct character', (done) => {
    const characterFields = {
      name: 'Test character',
      fields: [
        { name: 'charisma', value: 0 },
        { name: 'health', value: 0 },
        { name: 'luck', value: 0 },
        { name: 'magicalDefense', value: 0 },
        { name: 'magicalPower', value: 0 },
        { name: 'mana', value: 0 },
        { name: 'physicalDefense', value: 0 },
        { name: 'physicalPower', value: 0 },
        { name: 'speed', value: 0 },
        { name: 'stamina', value: 0 }
      ]
    }

    component.modelFields$.subscribe((fields) => {
      expect(fields).toEqual(characterFields)
      done()
    })
  })

  it('Should display 11 tr elements for properties of a character and one for the header', () => {
    expect(element.querySelectorAll('tr').length).toEqual(12)
  })
})
