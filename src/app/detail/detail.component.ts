import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { combineLatest, Observable } from 'rxjs'
import { filter, map, switchMap } from 'rxjs/operators'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'
import { GenericModelType } from '../models/generic-model-type-enum'

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  modelFields$: Observable<{
    name: string
    fields: { name: string; value: number }[]
  }>

  constructor(private route: ActivatedRoute, private modelService: GenericModelService) {}

  ngOnInit(): void {
    const modelType: Observable<GenericModelType> = this.route.pathFromRoot[1].url.pipe(
      map((urlSegments) => urlSegments[0].path),
      map((stringType) => GenericModelType[stringType])
    )
    this.modelFields$ = combineLatest([this.route.params, modelType]).pipe(
      filter(([params]) => params.id),
      switchMap(([params, type]) => this.modelService.getModel(params.id, type)),
      map(({ id, name, ...model }) => {
        const fields = Object.keys(model).map((field) => {
          return { name: field, value: model[field] }
        })
        fields.sort((fieldOne, fieldTwo) => fieldOne.name.localeCompare(fieldTwo.name))
        return {
          name,
          fields
        }
      })
    )
  }
}
