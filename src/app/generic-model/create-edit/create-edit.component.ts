import { Location } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { combineLatest, Observable, of } from 'rxjs'
import { map, switchMap, take } from 'rxjs/operators'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModelForm } from '../../models/genericModel.model'
import { GenericModelService } from '../../services/generic-model/generic-model.service'

@Component({
  selector: 'app-generic-model-create-edit',
  templateUrl: './create-edit.component.html'
})
export class CreateEditComponent implements OnInit {
  model: GenericModelForm = {
    name: '',
    health: 0,
    mana: 0,
    stamina: 0,
    physicalPower: 0,
    magicalPower: 0,
    physicalDefense: 0,
    magicalDefense: 0,
    speed: 0,
    luck: 0,
    charisma: 0
  }
  oldModel$: Observable<GenericModelForm>
  type: Observable<GenericModelType>

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private modelService: GenericModelService
  ) {}

  ngOnInit(): void {
    this.type = this.route.pathFromRoot[1].url.pipe(
      map((urlSegments) => urlSegments[0].path),
      map((stringType) => GenericModelType[stringType])
    )
    this.oldModel$ = combineLatest([this.route.params, this.type]).pipe(
      switchMap(([params, type]) => {
        if (params.id && typeof params.id === 'string') {
          return (this.modelService.getModelForm(params.id, type) as unknown) as Observable<GenericModelForm>
        } else {
          return of({
            name: '',
            health: 0,
            mana: 0,
            stamina: 0,
            physicalPower: 0,
            magicalPower: 0,
            physicalDefense: 0,
            magicalDefense: 0,
            speed: 0,
            luck: 0,
            charisma: 0
          })
        }
      })
    )
    this.resetForm()
  }

  onSubmit() {
    if (this.model.id) {
      this.type
        .pipe(take(1))
        .toPromise()
        .then((type) => this.modelService.updateModel(this.model, type))
    } else {
      this.type
        .pipe(take(1))
        .toPromise()
        .then((type) => this.modelService.createModel(this.model, type))
    }
    this.location.back()
  }

  async resetForm() {
    this.model = await this.oldModel$.pipe(take(1)).toPromise()
  }

  cancelForm() {
    this.location.back()
  }
}
