import { Location } from '@angular/common'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { GenericModelMockService } from 'src/app/services/generic-model/generic-model-mock.service'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'
import { CharacterMockService } from '../../services/character/character-mock.service'
import { CharacterService } from '../../services/character/character.service'

import { CreateEditComponent } from './create-edit.component'

describe('GenericModelCreateEditComponent', () => {
  let component: CreateEditComponent
  let fixture: ComponentFixture<CreateEditComponent>

  const locationStub = {
    back: jasmine.createSpy('back')
  }
  const activatedRouteStub = {
    pathFromRoot: [{}, { url: of([{ path: 'species' }]) }]
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateEditComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Location, useValue: locationStub }
      ],
      imports: [FormsModule, RouterTestingModule]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(CreateEditComponent, {
      set: {
        providers: [
          { provide: CharacterService, useClass: CharacterMockService },
          { provide: GenericModelService, useClass: GenericModelMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(CreateEditComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
