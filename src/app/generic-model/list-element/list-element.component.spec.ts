import { Component } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { By } from '@angular/platform-browser'
import { of } from 'rxjs'
import { EventType } from 'src/app/models/eventType-enum'
import { CharacterMockService } from 'src/app/services/character/character-mock.service'
import { ListElementComponent } from './list-element.component'

describe('ListElementComponent', () => {
  @Component({
    selector: 'app-host-component',
    template: `<tr
      generic-model-list-element
      [isAdmin]="true"
      [model]="{
        id: 'test',
        name: 'Test species form'
      }"
      (clickEvent)="handleEvent($event)"
    ></tr>`
  })
  class TestHostComponent {
    uid$ = of('testUser')

    constructor(public characterMock: CharacterMockService) {}

    handleEvent(event: { mode: EventType; id: string }) {}
  }

  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>
  let element: HTMLElement

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ListElementComponent, TestHostComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    element = fixture.nativeElement
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should have the details of the passed character on the screen', () => {
    const tdArray: HTMLTableCellElement[] = []
    element.querySelectorAll('td').forEach((td) => tdArray.push(td))

    expect(tdArray).toHaveSize(2)
    const texts = tdArray.map((td) => td.textContent)
    expect(texts).toContain('Test species form')
  })

  it('should have the buttons', () => {
    expect(element.querySelectorAll('button').length).toEqual(3)
  })

  it('should emit an event when button is pushed', () => {
    // Get the edit button with class btn-secondary
    const editButton = fixture.debugElement.query(By.css('.btn-secondary'))
    const handleEventSpy = spyOn(TestHostComponent.prototype, 'handleEvent')

    editButton.triggerEventHandler('click', null)
    fixture.detectChanges()

    fixture.whenStable().then(() => {
      expect(handleEventSpy).toHaveBeenCalledOnceWith({
        mode: EventType.edit,
        id: 'test'
      })
    })
  })
})
