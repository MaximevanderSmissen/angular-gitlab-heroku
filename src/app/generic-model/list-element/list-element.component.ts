import { Component, EventEmitter, Input, Output } from '@angular/core'
import { EventType } from 'src/app/models/eventType-enum'
import { GenericModelForm } from 'src/app/models/genericModel.model'

@Component({
  // tslint:disable-next-line
  selector: '[generic-model-list-element]',
  templateUrl: './list-element.component.html'
})
export class ListElementComponent {
  @Input() isAdmin: boolean
  @Input() model: GenericModelForm
  @Output() clickEvent: EventEmitter<{
    mode: EventType
    id: string
  }> = new EventEmitter()

  onClick(mode) {
    this.clickEvent.emit({ mode, id: this.model.id })
  }
}
