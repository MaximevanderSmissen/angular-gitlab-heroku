import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { GenericModelMockService } from 'src/app/services/generic-model/generic-model-mock.service'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'

import { ListComponent } from './list.component'

describe('GenericModelListComponent', () => {
  let component: ListComponent
  let fixture: ComponentFixture<ListComponent>

  const activatedRouteStub = {
    pathFromRoot: [
      {
        /* Ignored */
      },
      { url: of([{ path: 'species' }]) }
    ]
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [ListComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(ListComponent, {
      set: {
        providers: [
          { provide: ActivatedRoute, useValue: activatedRouteStub },
          { provide: GenericModelService, useClass: GenericModelMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(ListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
