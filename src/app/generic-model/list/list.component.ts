import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { Observable } from 'rxjs'
import { map, switchMap, take } from 'rxjs/operators'
import { EventType } from 'src/app/models/eventType-enum'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModelForm } from 'src/app/models/genericModel.model'
import { AuthService } from 'src/app/services/auth/auth.service'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'

@Component({
  selector: 'app-generic-model-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {
  isAdmin$: Observable<boolean>
  models$: Observable<GenericModelForm[]>
  type$: Observable<GenericModelType>

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private modelService: GenericModelService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.isAdmin$ = this.authService.isAdmin$
    this.type$ = this.route.pathFromRoot[1].url.pipe(
      map((urlSegments) => urlSegments[0].path),
      map((stringType) => GenericModelType[stringType])
    )
    this.models$ = this.type$.pipe(switchMap((type) => this.modelService.getModelForms(type)))
  }

  handleEvent(event: { mode: EventType; id: string }) {
    switch (event.mode) {
      case EventType.delete:
        this.type$
          .pipe(take(1))
          .toPromise()
          .then((type) => this.modelService.deleteModel(event.id, type))
        break
      case EventType.edit:
        this.router.navigate([`${event.id}/edit`], { relativeTo: this.route })
        break
      case EventType.filter:
        this.router.navigate([`${event.id}/compare`], { relativeTo: this.route })
        break

      default:
        throw new Error('Illegal mode passed')
    }
  }

  showModel(id: string) {
    this.router.navigate([id], { relativeTo: this.route })
  }

  createModel() {
    this.router.navigate(['create'], { relativeTo: this.route.parent })
  }
}
