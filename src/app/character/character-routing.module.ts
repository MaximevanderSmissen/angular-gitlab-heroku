import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { LoggedInGuard } from '../logged-in.guard'
import { CharactersComponent } from './characters/characters.component'
import { CompareComponent } from '../compare/compare.component'
import { CreateEditComponent } from './create-edit/create-edit.component'
import { DetailComponent } from '../detail/detail.component'
import { FilterComponent } from './filter/filter.component'
import { MyCharactersComponent } from './my-characters/my-characters.component'

const routes: Routes = [
  {
    path: 'character',
    children: [
      { path: 'create', component: CreateEditComponent, canActivate: [LoggedInGuard] },
      {
        path: 'mycharacters',
        component: MyCharactersComponent,
        children: [{ path: ':id', component: DetailComponent }]
      },
      { path: ':id/edit', component: CreateEditComponent },
      {
        path: ':id/compare',
        component: FilterComponent,
        children: [{ path: ':idOther', component: CompareComponent }]
      },
      { path: '', component: CharactersComponent, children: [{ path: ':id', component: DetailComponent }] }
    ]
  },
  { path: '**', redirectTo: '/' }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CharacterRoutingModule {}
