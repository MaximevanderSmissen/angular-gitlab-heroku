import { ComponentFixture, TestBed } from '@angular/core/testing'
import { FormBuilder, FormsModule } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'

import { FilterComponent } from './filter.component'

describe('FilterComponent', () => {
  let component: FilterComponent
  let fixture: ComponentFixture<FilterComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FilterComponent],
      providers: [FormBuilder]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(FilterComponent, {
      set: {
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              params: of({ id: 'test' })
            }
          }
        ]
      }
    })
    fixture = TestBed.createComponent(FilterComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
