import { Component, OnInit } from '@angular/core'
import { FormBuilder, Validators } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { BehaviorSubject, combineLatest, Observable, Subscription } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { CharacterForm } from 'src/app/models/character.model'
import { FilterType } from 'src/app/models/filterType-enum'
import { CharacterService } from 'src/app/services/character/character.service'

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  characters$: Observable<CharacterForm[]>
  filterForm = this.fb.group({
    filterTypeCtrl: ['', [Validators.required]]
  })
  filterTypes = FilterType
  subscription: Subscription

  constructor(
    private route: ActivatedRoute,
    public characterService: CharacterService,
    public fb: FormBuilder
  ) {}

  ngOnInit(): void {
    const charOne$ = this.route.params.pipe(
      switchMap((params) => {
        if (params.id) {
          return this.characterService.getModelForm(params.id)
        }
      })
    )
    const filterType$ = this.filterTypeCtrl.valueChanges.pipe(
      map((value) => {
        switch (value) {
          case FilterType.role:
            return FilterType.role
          case FilterType.similar:
            return FilterType.similar
          case FilterType.species:
            return FilterType.species
          default:
            return FilterType.similar
        }
      })
    )
    const filterSubject$ = new BehaviorSubject(FilterType.similar)
    this.subscription = filterType$.subscribe(filterSubject$)
    this.characters$ = combineLatest([filterSubject$, charOne$]).pipe(
      switchMap(([filterType, character]) => this.characterService.filterCharacters(filterType, character))
    )
    this.filterTypeCtrl.setValue(FilterType.similar)
  }

  get filterTypeCtrl() {
    return this.filterForm.get('filterTypeCtrl')
  }

  filter() {}
}
