import { Component } from '@angular/core'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { AuthMockService } from 'src/app/services/auth/auth-mock.service'
import { AuthService } from 'src/app/services/auth/auth.service'
import { CharacterMockService } from 'src/app/services/character/character-mock.service'
import { CharacterService } from 'src/app/services/character/character.service'

import { CharacterListComponent } from './character-list.component'

describe('CharacterListComponent', () => {
  @Component({
    selector: 'app-host-component',
    template: `<app-character-list [characters$]="characterMock.characterForms$"></app-character-list>`
  })
  class TestHostComponent {
    constructor(public characterMock: CharacterMockService) {}
  }

  let component: TestHostComponent
  let fixture: ComponentFixture<TestHostComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [CharacterListComponent, TestHostComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(CharacterListComponent, {
      set: {
        providers: [
          { provide: ActivatedRoute, useValue: {} },
          { provide: AuthService, useClass: AuthMockService },
          { provide: CharacterService, useClass: CharacterMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(TestHostComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
