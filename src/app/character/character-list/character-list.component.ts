import { Component, Input, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { EMPTY, Observable, of } from 'rxjs'
import { switchMap } from 'rxjs/operators'
import { CharacterForm } from 'src/app/models/character.model'
import { EventType } from 'src/app/models/eventType-enum'
import { AuthService } from 'src/app/services/auth/auth.service'
import { CharacterService } from 'src/app/services/character/character.service'

@Component({
  selector: 'app-character-list',
  templateUrl: './character-list.component.html',
  styleUrls: ['./character-list.component.scss']
})
export class CharacterListComponent implements OnInit {
  @Input() characters$: Observable<CharacterForm[]>
  uid$: Observable<string>

  constructor(
    private route: ActivatedRoute,
    public authService: AuthService,
    public characterService: CharacterService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.uid$ = this.authService.user$.pipe(switchMap((user) => (user ? of(user.uid) : EMPTY)))
  }

  handleEvent(event: { mode: EventType; id: string }) {
    switch (event.mode) {
      case EventType.delete:
        this.characterService.deleteModel(event.id)
        break
      case EventType.edit:
        this.router.navigate([`/character/${event.id}/edit`])
        break
      case EventType.filter:
        this.router.navigate([`/character/${event.id}/compare`])
        break

      default:
        throw new Error('Illegal mode passed')
    }
  }

  showCharacter(id: string) {
    this.router.navigate([id], { relativeTo: this.route })
  }

  createCharacter() {
    this.router.navigate(['create'], { relativeTo: this.route.parent })
  }
}
