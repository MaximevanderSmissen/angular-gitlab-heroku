import { Component, OnInit } from '@angular/core'
import { CharacterService } from 'src/app/services/character/character.service'

@Component({
  selector: 'app-my-characters',
  templateUrl: './my-characters.component.html',
  styleUrls: ['./my-characters.component.scss']
})
export class MyCharactersComponent implements OnInit {
  constructor(public characterService: CharacterService) {}

  ngOnInit(): void {}
}
