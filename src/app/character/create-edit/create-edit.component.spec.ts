import { Location } from '@angular/common'
import { ComponentFixture, TestBed } from '@angular/core/testing'
import { FormsModule } from '@angular/forms'
import { ActivatedRoute } from '@angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { of } from 'rxjs'
import { CharacterMockService } from 'src/app/services/character/character-mock.service'
import { CharacterService } from 'src/app/services/character/character.service'
import { GenericModelMockService } from 'src/app/services/generic-model/generic-model-mock.service'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'

import { CreateEditComponent } from './create-edit.component'

describe('CreateEditComponent', () => {
  let component: CreateEditComponent
  let fixture: ComponentFixture<CreateEditComponent>

  const locationStub = {
    back: jasmine.createSpy('back')
  }
  const activatedRouteStub = {
    pathFromRoot: [{}, { url: of([{ path: 'character' }]) }],
    params: of({})
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CreateEditComponent],
      providers: [
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: Location, useValue: locationStub }
      ],
      imports: [FormsModule, RouterTestingModule]
    }).compileComponents()
  })

  beforeEach(() => {
    spyOn(CharacterMockService.prototype, 'getModelForm')
    spyOn(CharacterMockService.prototype, 'createModel')
    spyOn(CharacterMockService.prototype, 'updateModel')
    TestBed.overrideComponent(CreateEditComponent, {
      set: {
        providers: [
          { provide: CharacterService, useClass: CharacterMockService },
          { provide: GenericModelService, useClass: GenericModelMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(CreateEditComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })

  it('should set model on reset form according to data in oldModel$', async (done) => {
    // Assign oldModel and check that model is default empty model
    component.oldModel$ = of({
      id: '1',
      name: 'Test character form',
      speciesId: '1',
      roleId: '1'
    })
    expect(component.model).toEqual({
      name: '',
      speciesId: '',
      roleId: ''
    })
    // Simulate form reset
    await component.resetForm()
    // Assert
    expect(component.model).toEqual({
      id: '1',
      name: 'Test character form',
      speciesId: '1',
      roleId: '1'
    })
    done()
  })

  it('should create character when onSubmit clicked on model without id', () => {
    // Check model is empty
    expect(component.model).toEqual({
      name: '',
      speciesId: '',
      roleId: ''
    })
    // Simulate submit
    component.onSubmit()
    // Assert
    expect(CharacterMockService.prototype.createModel).toHaveBeenCalled()
    expect(locationStub.back).toHaveBeenCalled()
  })

  it('should update character when onSubmit clicked on model with id', async (done) => {
    // Set model through oldModel and reset
    component.model = {
      id: '1',
      name: 'Test character form',
      speciesId: '1',
      roleId: '1'
    }
    // Simulate submit
    component.onSubmit()
    // Assert
    await expect(CharacterMockService.prototype.updateModel).toHaveBeenCalled()
    done()
  })
})
