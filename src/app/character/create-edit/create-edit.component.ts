import { Location } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Observable, of } from 'rxjs'
import { switchMap, take } from 'rxjs/operators'
import { CharacterForm } from 'src/app/models/character.model'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModel } from 'src/app/models/genericModel.model'
import { CharacterService } from 'src/app/services/character/character.service'
import { GenericModelService } from 'src/app/services/generic-model/generic-model.service'

@Component({
  selector: 'app-create-edit',
  templateUrl: './create-edit.component.html',
  styleUrls: ['./create-edit.component.scss']
})
export class CreateEditComponent implements OnInit {
  model: CharacterForm = {
    name: '',
    speciesId: '',
    roleId: ''
  }
  oldModel$: Observable<CharacterForm>
  roles$: Observable<GenericModel[]>
  species$: Observable<GenericModel[]>

  constructor(
    private route: ActivatedRoute,
    private characterService: CharacterService,
    private location: Location,
    private genericModelService: GenericModelService
  ) {}

  ngOnInit(): void {
    this.roles$ = this.genericModelService.getModels(GenericModelType.role)
    this.species$ = this.genericModelService.getModels(GenericModelType.species)
    this.oldModel$ = this.route.params.pipe(
      switchMap((params) => {
        if (params.id && typeof params.id === 'string') {
          return this.characterService.getModelForm(params.id)
        } else {
          return of({
            name: '',
            speciesId: '',
            roleId: ''
          })
        }
      })
    )
    this.resetForm()
  }

  onSubmit() {
    if (this.model.id) {
      this.characterService.updateModel(this.model)
    } else {
      this.characterService.createModel(this.model)
    }
    this.location.back()
  }

  async resetForm() {
    this.model = await this.oldModel$.pipe(take(1)).toPromise()
  }

  cancelForm() {
    this.location.back()
  }
}
