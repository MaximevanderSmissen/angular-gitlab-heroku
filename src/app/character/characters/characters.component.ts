import { Component, OnInit } from '@angular/core'
import { CharacterService } from 'src/app/services/character/character.service'

@Component({
  selector: 'app-characters',
  templateUrl: './characters.component.html',
  styleUrls: ['./characters.component.scss']
})
export class CharactersComponent implements OnInit {
  constructor(public characterService: CharacterService) {}

  ngOnInit(): void {}
}
