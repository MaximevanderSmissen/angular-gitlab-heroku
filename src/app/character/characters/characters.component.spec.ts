import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute } from '@angular/router'
import { CharacterMockService } from 'src/app/services/character/character-mock.service'
import { CharacterService } from 'src/app/services/character/character.service'

import { CharactersComponent } from './characters.component'

describe('CharactersComponent', () => {
  let component: CharactersComponent
  let fixture: ComponentFixture<CharactersComponent>

  const activatedRouteStub = {
    navigate: jasmine.createSpy('navigate')
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CharactersComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(CharactersComponent, {
      set: {
        providers: [
          { provide: CharacterService, useClass: CharacterMockService },
          { provide: ActivatedRoute, useValue: activatedRouteStub }
        ]
      }
    })
    fixture = TestBed.createComponent(CharactersComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
