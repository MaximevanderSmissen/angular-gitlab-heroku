import { Component, EventEmitter, Input, Output } from '@angular/core'
import { Observable } from 'rxjs'
import { CharacterForm } from 'src/app/models/character.model'
import { EventType } from 'src/app/models/eventType-enum'

@Component({
  // tslint:disable-next-line
  selector: '[character-list-element]',
  templateUrl: './list-element.component.html',
  styleUrls: ['./list-element.component.scss']
})
export class ListElementComponent {
  @Input() character: CharacterForm
  @Input() uid$: Observable<string>
  @Output() clickEvent: EventEmitter<{
    mode: EventType
    id: string
  }> = new EventEmitter()

  onClick(mode) {
    this.clickEvent.emit({ mode, id: this.character.id })
  }
}
