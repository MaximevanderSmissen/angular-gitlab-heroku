import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { CharacterRoutingModule } from './character-routing.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CreateEditComponent } from './create-edit/create-edit.component'
import { ListElementComponent } from './list-element/list-element.component'
import { CharacterListComponent } from './character-list/character-list.component'
import { MyCharactersComponent } from './my-characters/my-characters.component'
import { DetailComponent } from '../detail/detail.component'
import { CharactersComponent } from './characters/characters.component'
import { FilterComponent } from './filter/filter.component'

@NgModule({
  declarations: [
    CreateEditComponent,
    ListElementComponent,
    CharacterListComponent,
    MyCharactersComponent,
    DetailComponent,
    CharactersComponent,
    FilterComponent
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, CharacterRoutingModule]
})
export class CharacterModule {}
