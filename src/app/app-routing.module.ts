import { NgModule } from '@angular/core'
import { Routes, RouterModule } from '@angular/router'
import { UsecasesComponent } from './about/usecases/usecases.component'
import { BrowserModule } from '@angular/platform-browser'
import { UsecasesListComponent } from './about/usecases/usecases-list/usecases-list.component'
import { UsecaseComponent } from './about/usecases/usecase/usecase.component'
import { CompareComponent } from './compare/compare.component'
import { DetailComponent } from './detail/detail.component'
import { CreateEditComponent } from './generic-model/create-edit/create-edit.component'
import { ListComponent } from './generic-model/list/list.component'

const routes: Routes = [
  { path: '', redirectTo: 'character', pathMatch: 'full' },
  {
    path: 'role',
    children: [
      { path: 'create', component: CreateEditComponent },
      { path: ':id/edit', component: CreateEditComponent },
      {
        path: ':id/compare',
        component: ListComponent,
        children: [{ path: ':idOther', component: CompareComponent }]
      },
      { path: '', component: ListComponent, children: [{ path: ':id', component: DetailComponent }] }
    ]
  },
  {
    path: 'species',
    children: [
      { path: 'create', component: CreateEditComponent },
      { path: ':id/edit', component: CreateEditComponent },
      {
        path: ':id/compare',
        component: ListComponent,
        children: [{ path: ':idOther', component: CompareComponent }]
      },
      { path: '', component: ListComponent, children: [{ path: ':id', component: DetailComponent }] }
    ]
  },
  { path: 'about', component: UsecasesComponent },
  {
    path: 'usecases',
    component: UsecasesListComponent,
    children: [
      { path: '', component: UsecaseComponent },
      { path: ':id', component: UsecaseComponent }
    ]
  }
]

@NgModule({
  imports: [BrowserModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
