export interface User {
  uid: string
  email: string
  displayName?: string
  photoURL?: string
  activated: boolean
  isAdmin?: boolean
}
