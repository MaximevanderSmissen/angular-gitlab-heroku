export interface UseCase {
  // Optioneel description attribuut
  id?: string
  description?: string
  name: string
  actor: string
  precondition: string
  postcondition: string
  scenario: string[]
}
