export enum GenericModelType {
  character = 'characters',
  species = 'species',
  role = 'roles'
}
