export enum EventType {
  delete = 'delete',
  edit = 'edit',
  filter = 'filter'
}
