import { GenericModel, GenericModelForm } from './genericModel.model'

export interface Character extends GenericModel {
  uid: string
}

export interface CharacterForm extends GenericModelForm {
  uid?: string
  speciesId: string
  speciesName?: string
  roleId: string
  roleName?: string
}
