export interface GenericModel {
  id: string
  name: string
  health: number
  mana: number
  stamina: number
  physicalPower: number
  magicalPower: number
  physicalDefense: number
  magicalDefense: number
  speed: number
  luck: number
  charisma: number
}

export interface GenericModelForm {
  id?: string
  name: string
  health?: number
  mana?: number
  stamina?: number
  physicalPower?: number
  magicalPower?: number
  physicalDefense?: number
  magicalDefense?: number
  speed?: number
  luck?: number
  charisma?: number
}
