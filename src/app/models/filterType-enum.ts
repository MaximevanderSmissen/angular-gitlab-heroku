export enum FilterType {
  similar = 'similar',
  species = 'species',
  role = 'role'
}
