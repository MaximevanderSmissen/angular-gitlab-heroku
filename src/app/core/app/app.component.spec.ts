import { TestBed, waitForAsync } from '@angular/core/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { AppComponent } from './app.component'
import { UsecasesComponent } from '../../about/usecases/usecases.component'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'
import { UsecaseComponent } from 'src/app/about/usecases/usecase/usecase.component'
import { NavbarComponent } from '../navbar/navbar.component'
import { AuthService } from 'src/app/services/auth/auth.service'
import { AuthMockService } from 'src/app/services/auth/auth-mock.service'

describe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule, NgbModule],
        declarations: [AppComponent, NavbarComponent, UsecasesComponent, UsecaseComponent]
      }).compileComponents()
      TestBed.overrideComponent(NavbarComponent, {
        set: { providers: [{ provide: AuthService, useClass: AuthMockService }] }
      })
    })
  )

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app).toBeTruthy()
  })

  it(`should have as title 'Valemnia: The blood moon'`, () => {
    const fixture = TestBed.createComponent(AppComponent)
    const app = fixture.debugElement.componentInstance
    expect(app.title).toEqual('Valemnia: The blood moon')
  })
})
