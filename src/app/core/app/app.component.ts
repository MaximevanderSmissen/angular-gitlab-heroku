import { Component } from '@angular/core'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  shortTitle = 'Valemnia'
  title = 'Valemnia: The blood moon'
}
