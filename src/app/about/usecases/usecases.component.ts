import { Component, OnInit } from '@angular/core'

@Component({
  selector: 'app-about-usecases',
  templateUrl: './usecases.component.html',
  styleUrls: ['./usecases.component.scss']
})
export class UsecasesComponent implements OnInit {
  readonly TITLE = 'Valemnia: The blood moon'
  readonly NAME = 'Maxime van der Smissen'
  readonly STUDENT_NUMBER = 2143045
  readonly PLAIN_USER = 'Reguliere gebruiker'
  readonly ADMIN_USER = 'Administrator'

  constructor() {}

  ngOnInit() {}
}
