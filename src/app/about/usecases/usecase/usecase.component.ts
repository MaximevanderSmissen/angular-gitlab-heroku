import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Subscription } from 'rxjs'
import { UsecaseService } from 'src/app/services/usecase/usecase.service'
import { UseCase } from 'src/app/models/usecase.model'

@Component({
  selector: 'app-usecase',
  templateUrl: './usecase.component.html'
})
export class UsecaseComponent implements OnInit, OnDestroy {
  useCase: UseCase
  subscription$: Subscription

  constructor(private route: ActivatedRoute, private usecaseService: UsecaseService) {}

  ngOnInit() {
    this.route.params.subscribe((params) => {
      if (params.id) {
        this.subscription$ = this.usecaseService.getUsecase(params.id).subscribe((usecase) => {
          this.useCase = usecase
        })
      }
    })
  }

  ngOnDestroy() {
    if (this.subscription$) {
      this.subscription$.unsubscribe()
    }
  }
}
