import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing'

import { UsecasesListComponent } from './usecases-list.component'
import { RouterTestingModule } from '@angular/router/testing'
import { UsecaseService } from 'src/app/services/usecase/usecase.service'
import { MockUsecaseService } from 'src/app/services/usecase/mock-usecase.service'

describe('UsecasesListComponent', () => {
  let component: UsecasesListComponent
  let fixture: ComponentFixture<UsecasesListComponent>

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [UsecasesListComponent]
      }).compileComponents()
    })
  )

  beforeEach(() => {
    TestBed.overrideComponent(UsecasesListComponent, {
      set: { providers: [{ provide: UsecaseService, useClass: MockUsecaseService }] }
    })
    fixture = TestBed.createComponent(UsecasesListComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
