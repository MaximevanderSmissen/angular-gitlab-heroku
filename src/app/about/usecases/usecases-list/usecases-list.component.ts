import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { Observable } from 'rxjs'
import { UseCase } from 'src/app/models/usecase.model'
import { UsecaseService } from 'src/app/services/usecase/usecase.service'

@Component({
  selector: 'app-usecases-list',
  templateUrl: './usecases-list.component.html',
  styleUrls: ['./usecases-list.component.scss']
})
export class UsecasesListComponent implements OnInit {
  title = 'Usecases'
  useCases: Observable<UseCase[]>

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private usecaseService: UsecaseService
  ) {}

  ngOnInit() {
    this.useCases = this.usecaseService.getUsecases()
  }

  showUseCase(id: UseCase) {
    this.router.navigate([id], { relativeTo: this.route })
  }
}
