import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { combineLatest, EMPTY, Observable } from 'rxjs'
import { map, switchMap } from 'rxjs/operators'
import { GenericModelType } from '../models/generic-model-type-enum'
import { GenericModelService } from '../services/generic-model/generic-model.service'

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit {
  fields$: Observable<{
    nameOne: string
    nameTwo: string
    fields: { name: string; valueOne: any; valueTwo: any }[]
  }>

  constructor(private route: ActivatedRoute, private modelService: GenericModelService) {}

  ngOnInit(): void {
    const comparableType: Observable<GenericModelType> = this.route.pathFromRoot[1].url.pipe(
      map((urlSegments) => urlSegments[0].path),
      map((stringType) => GenericModelType[stringType])
    )
    const modelOne$ = combineLatest([this.route.parent.params, comparableType]).pipe(
      switchMap(([params, type]) => {
        if (params.id) {
          return this.modelService.getModel(params.id, type)
        } else {
          return EMPTY
        }
      })
    )
    const modelTwo$ = combineLatest([this.route.params, comparableType]).pipe(
      switchMap(([params, type]) => {
        if (params.idOther) {
          return this.modelService.getModel(params.idOther, type)
        } else {
          return EMPTY
        }
      })
    )
    this.fields$ = combineLatest([modelOne$, modelTwo$]).pipe(
      map(([{ id: _0, name: nameOne, ...fieldsOne }, { id: _2, name: nameTwo, ...fieldsTwo }]) => {
        const fields = Object.keys(fieldsOne).map((field) => {
          return { name: field, valueOne: fieldsOne[field], valueTwo: fieldsTwo[field] }
        })
        fields.sort((fieldOne, fieldTwo) => fieldOne.name.localeCompare(fieldTwo.name))
        return { nameOne, nameTwo, fields }
      })
    )
  }
}
