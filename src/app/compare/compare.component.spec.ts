import { ComponentFixture, TestBed } from '@angular/core/testing'
import { ActivatedRoute } from '@angular/router'
import { of } from 'rxjs'
import { GenericModelMockService } from '../services/generic-model/generic-model-mock.service'
import { GenericModelService } from '../services/generic-model/generic-model.service'
import { CompareComponent } from './compare.component'

describe('CompareComponent', () => {
  let component: CompareComponent
  let fixture: ComponentFixture<CompareComponent>

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CompareComponent]
    }).compileComponents()
  })

  beforeEach(() => {
    TestBed.overrideComponent(CompareComponent, {
      set: {
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              params: of({ idOther: 'test' }),
              parent: {
                params: of({ id: 'test2' })
              },
              pathFromRoot: [
                {
                  /* Ignored */
                },
                {
                  url: of([{ path: 'character' }])
                }
              ]
            }
          },
          { provide: GenericModelService, useClass: GenericModelMockService }
        ]
      }
    })
    fixture = TestBed.createComponent(CompareComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
