import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export abstract class ModelService<Model, ModelForm> {
  abstract getModels(): Observable<Model[]>
  abstract getModelForms(): Observable<ModelForm[]>
  abstract getModel(id: string): Observable<Model>
  abstract getModelForm(id: string): Observable<ModelForm>
  abstract createModel(formModel: ModelForm): Promise<void>
  abstract deleteModel(id: string): Promise<void>
  abstract updateModel(model: ModelForm): Promise<void>
}
