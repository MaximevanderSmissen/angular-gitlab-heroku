import { Injectable } from '@angular/core'
import { AngularFirestore, QueryFn } from '@angular/fire/firestore'
import { EMPTY, Observable } from 'rxjs'
import { switchMap, take } from 'rxjs/operators'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { FilterType } from 'src/app/models/filterType-enum'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModel } from 'src/app/models/genericModel.model'
import { AuthService } from '../auth/auth.service'
import { CharacterService } from './character.service'

@Injectable({
  providedIn: 'root'
})
export class CharacterFirebaseService implements CharacterService {
  constructor(private auth: AuthService, private afs: AngularFirestore) {}

  private characters$: Observable<Character[]> = this.afs
    .collection<CharacterForm>(GenericModelType.character)
    .valueChanges({ idField: 'id' })
    .pipe(switchMap((characters) => this.toCharacters(characters)))
  private characterForms$: Observable<CharacterForm[]> = this.afs
    .collection<CharacterForm>(GenericModelType.character)
    .valueChanges({ idField: 'id' })
    .pipe(switchMap((characters) => this.toCharacterForms(characters)))
  myCharacters$: Observable<Character[]> = this.auth.user$.pipe(
    switchMap((user) => {
      if (user) {
        return this.afs
          .collection<CharacterForm>(GenericModelType.character, (ref) => ref.where('uid', '==', user.uid))
          .valueChanges({ idField: 'id' })
      } else {
        return EMPTY
      }
    }),
    switchMap((characters) => this.toCharacters(characters))
  )
  myCharacterForms$: Observable<CharacterForm[]> = this.auth.user$.pipe(
    switchMap((user) => {
      if (user) {
        return this.afs
          .collection<CharacterForm>(GenericModelType.character, (ref) => ref.where('uid', '==', user.uid))
          .valueChanges({ idField: 'id' })
      } else {
        return EMPTY
      }
    }),
    switchMap((characters) => this.toCharacterForms(characters))
  )

  getModels(): Observable<Character[]> {
    return this.characters$
  }
  getModelForms(): Observable<CharacterForm[]> {
    return this.characterForms$
  }
  getModel(id: string): Observable<Character> {
    return this.getCharacter(id)
  }
  getModelForm(id: string): Observable<CharacterForm> {
    return this.getCharacterForm(id)
  }

  async createModel({ id, ...formModel }: CharacterForm): Promise<void> {
    const user = await this.auth.user$.pipe(take(1)).toPromise()
    const data = {
      ...formModel,
      uid: user.uid
    }
    await this.afs.collection(GenericModelType.character).add(data)
  }
  private getCharacter(id: string): Observable<Character> {
    return this.afs
      .doc<CharacterForm & { id: string }>(`characters/${id}`)
      .valueChanges({ idField: 'id' })
      .pipe(
        switchMap(async (model) => {
          if (model) {
            const role$ = this.afs.doc<GenericModel>(`roles/${model.roleId}`).valueChanges({ idField: 'id' })
            const role = await role$.pipe(take(1)).toPromise()
            const spec$ = this.afs
              .doc<GenericModel>(`species/${model.speciesId}`)
              .valueChanges({ idField: 'id' })
            const spec = await spec$.pipe(take(1)).toPromise()
            return this.toCharacter(model, role, spec)
          } else {
            return null
          }
        })
      )
  }
  private getCharacterForm(id: string): Observable<CharacterForm> {
    return this.afs
      .doc<CharacterForm & { id: string }>(`characters/${id}`)
      .valueChanges({ idField: 'id' })
      .pipe(
        switchMap(async (model: CharacterForm & { id: string }) => {
          if (model) {
            const role$ = this.afs.doc<GenericModel>(`roles/${model.roleId}`).valueChanges({ idField: 'id' })
            const role = await role$.pipe(take(1)).toPromise()
            const spec$ = this.afs
              .doc<GenericModel>(`species/${model.speciesId}`)
              .valueChanges({ idField: 'id' })
            const spec = await spec$.pipe(take(1)).toPromise()
            return this.toCharacterForm(model, role, spec)
          } else {
            return null
          }
        })
      )
  }
  async deleteModel(id: string) {
    await this.afs.doc(`characters/${id}`).delete()
  }

  async updateModel(model: CharacterForm) {
    await this.afs.doc<CharacterForm>(`characters/${model.id}`).set(model)
  }

  filterCharacters(filter: FilterType, character: CharacterForm) {
    const query = this.toQuery(filter, character)
    return this.afs
      .collection<CharacterForm>(GenericModelType.character, query)
      .valueChanges({ idField: 'id' })
      .pipe(switchMap((characters) => this.toCharacterForms(characters)))
  }

  private toCharacter(
    model: CharacterForm & { id: string },
    role: GenericModel,
    species: GenericModel
  ): Character {
    // Add up all the stats
    const { id, name, ...stats } = role
    for (const key of Object.keys(stats)) {
      stats[key] += species[key]
    }
    const char: Character = {
      id: model.id,
      name: model.name,
      uid: model.uid,
      ...stats
    }
    return char
  }

  private toCharacterForm(
    model: CharacterForm & { id: string },
    role: GenericModel,
    species: GenericModel
  ): CharacterForm {
    return { ...model, roleName: role.name, speciesName: species.name }
  }

  private async toCharacters(characters: (CharacterForm & { id: string })[]): Promise<Character[]> {
    const species = await this.afs
      .collection<GenericModel>(GenericModelType.species, (ref) => ref.orderBy('name'))
      .valueChanges({ idField: 'id' })
      .pipe(take(1))
      .toPromise()
    const roles = await this.afs
      .collection<GenericModel>(GenericModelType.role, (ref) => ref.orderBy('name'))
      .valueChanges({ idField: 'id' })
      .pipe(take(1))
      .toPromise()
    return characters.map((character) => {
      const role = roles.find((rol) => rol.id === character.roleId)
      const spec = species.find((specie) => specie.id === character.speciesId)
      return this.toCharacter(character, role, spec)
    })
  }

  private async toCharacterForms(characters: (CharacterForm & { id: string })[]): Promise<CharacterForm[]> {
    const species = await this.afs
      .collection<GenericModel>(GenericModelType.species, (ref) => ref.orderBy('name'))
      .valueChanges({ idField: 'id' })
      .pipe(take(1))
      .toPromise()
    const roles = await this.afs
      .collection<GenericModel>(GenericModelType.role, (ref) => ref.orderBy('name'))
      .valueChanges({ idField: 'id' })
      .pipe(take(1))
      .toPromise()
    return characters.map((character) => {
      const role = roles.find((rol) => rol.id === character.roleId)
      const spec = species.find((specie) => specie.id === character.speciesId)
      return this.toCharacterForm(character, role, spec)
    })
  }

  private toQuery(filter: FilterType, character: CharacterForm): QueryFn {
    switch (filter) {
      case FilterType.species:
        return (ref) => ref.where('speciesId', '==', character.speciesId)
      case FilterType.role:
        return (ref) => ref.where('roleId', '==', character.roleId)
      case FilterType.similar:
        return (ref) =>
          ref.where('speciesId', '==', character.speciesId).where('roleId', '==', character.roleId)
    }
  }
}
