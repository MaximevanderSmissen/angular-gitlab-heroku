import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { FilterType } from 'src/app/models/filterType-enum'
import { ModelService } from '../model.service'

@Injectable({
  providedIn: 'root'
})
export abstract class CharacterService extends ModelService<Character, CharacterForm> {
  abstract myCharacters$: Observable<Character[]>
  abstract myCharacterForms$: Observable<CharacterForm[]>
  abstract filterCharacters(filterType: FilterType, character: CharacterForm): Observable<CharacterForm[]>
}
