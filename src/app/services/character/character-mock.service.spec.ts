import { TestBed } from '@angular/core/testing'

import { CharacterMockService } from './character-mock.service'

describe('CharacterMockService', () => {
  let service: CharacterMockService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(CharacterMockService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
