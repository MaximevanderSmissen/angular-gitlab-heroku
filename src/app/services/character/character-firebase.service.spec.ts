import { TestBed } from '@angular/core/testing'
import { AngularFireModule } from '@angular/fire'
import { AngularFirestore } from '@angular/fire/firestore'
import { take } from 'rxjs/operators'
import { CharacterForm } from 'src/app/models/character.model'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { environment } from 'src/environments/environment'
import { AuthMockService } from '../auth/auth-mock.service'
import { AuthService } from '../auth/auth.service'

import { CharacterFirebaseService } from './character-firebase.service'

describe('CharacterFirebaseService', () => {
  let service: CharacterFirebaseService
  let afs: AngularFirestore
  const characterData = {
    id: 'nMZPCi0oXYebHkoAnqJp',
    uid: '1',
    speciesId: 'nMZPCi0oXYebHkoAnqJp',
    roleId: 'nMZPCi0oXYebHkoAnqJp',
    name: 'Test Character'
  }
  const characterReadData = {
    id: 'nMZPCi0oXYebHkoAnqJp',
    uid: '1',
    name: 'Test Character',
    health: 2,
    mana: 2,
    stamina: 2,
    physicalPower: 2,
    magicalPower: 2,
    physicalDefense: 2,
    magicalDefense: 2,
    speed: 2,
    luck: 2,
    charisma: 2
  }
  const speciesRoleData = {
    health: 1,
    mana: 1,
    stamina: 1,
    physicalPower: 1,
    magicalPower: 1,
    physicalDefense: 1,
    magicalDefense: 1,
    speed: 1,
    luck: 1,
    charisma: 1
  }
  const speciesData = { ...speciesRoleData, name: 'Test Species' }
  const roleData = { ...speciesRoleData, name: 'Test Role' }

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireModule.initializeApp(environment.firebase)],
      providers: [{ provide: AuthService, useClass: AuthMockService }]
    })
    service = TestBed.inject(CharacterFirebaseService)
    afs = TestBed.inject(AngularFirestore)
  })

  beforeEach(async (done) => {
    // Setup character to test with
    await afs.collection(GenericModelType.character).doc('nMZPCi0oXYebHkoAnqJp').set(characterData)
    await afs.collection(GenericModelType.species).doc('nMZPCi0oXYebHkoAnqJp').set(speciesData)
    await afs.collection(GenericModelType.role).doc('nMZPCi0oXYebHkoAnqJp').set(roleData)
    done()
  })

  afterEach(async (done) => {
    await afs.doc('characters/nMZPCi0oXYebHkoAnqJp').delete()
    await afs.doc('species/nMZPCi0oXYebHkoAnqJp').delete()
    await afs.doc('roles/nMZPCi0oXYebHkoAnqJp').delete()
    done()
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })

  it('should get the right character', async (done) => {
    const character = await service.getModel('nMZPCi0oXYebHkoAnqJp').pipe(take(1)).toPromise()
    expect(character).toEqual(characterReadData)
    done()
  })

  it('should create a new character', async (done) => {
    // Setup and check that test user has 1 character
    const testCharacters = afs
      .collection<CharacterForm>(GenericModelType.character, (ref) => ref.where('uid', '==', '1'))
      .valueChanges({ idField: 'id' })
    expect(await testCharacters.pipe(take(1)).toPromise()).toHaveSize(1)
    // Create a character
    await service.createModel({
      speciesId: 'nMZPCi0oXYebHkoAnqJp',
      roleId: 'nMZPCi0oXYebHkoAnqJp',
      name: 'Test Character'
    })
    // Assert a character was created
    const result = await testCharacters.pipe(take(1)).toPromise()
    expect(result).toHaveSize(2)
    // Delete to cleanup
    result.forEach(({ id }) => {
      afs.doc(`characters/${id}`).delete()
    })
    done()
  })

  it('should update an existing character', async (done) => {
    // Setup and check that test user has 1 character
    const testCharacter = afs
      .doc<CharacterForm & { id: string }>('characters/nMZPCi0oXYebHkoAnqJp')
      .valueChanges({ idField: 'id' })
    expect(await testCharacter.pipe(take(1)).toPromise()).toEqual({
      id: 'nMZPCi0oXYebHkoAnqJp',
      speciesId: 'nMZPCi0oXYebHkoAnqJp',
      roleId: 'nMZPCi0oXYebHkoAnqJp',
      name: 'Test Character',
      uid: '1'
    })
    // update character
    await service.updateModel({
      id: 'nMZPCi0oXYebHkoAnqJp',
      speciesId: 'nMZPCi0oXYebHkoAnqJp',
      roleId: 'nMZPCi0oXYebHkoAnqJp',
      name: 'Test Character new',
      uid: '1'
    })
    // Assert a character was created
    expect(await testCharacter.pipe(take(1)).toPromise()).toEqual({
      id: 'nMZPCi0oXYebHkoAnqJp',
      speciesId: 'nMZPCi0oXYebHkoAnqJp',
      roleId: 'nMZPCi0oXYebHkoAnqJp',
      name: 'Test Character new',
      uid: '1'
    })
    done()
  })

  it('should delete character', async (done) => {
    // Setup and check that test user has 1 character
    const testCharacters = afs
      .collection<CharacterForm>(GenericModelType.character, (ref) => ref.where('uid', '==', '1'))
      .valueChanges({ idField: 'id' })
    expect(await testCharacters.pipe(take(1)).toPromise()).toHaveSize(1)
    // delete character
    await service.deleteModel('nMZPCi0oXYebHkoAnqJp')
    // Assert a character was deleted
    expect(await testCharacters.pipe(take(1)).toPromise()).toHaveSize(0)
    done()
  })
})
