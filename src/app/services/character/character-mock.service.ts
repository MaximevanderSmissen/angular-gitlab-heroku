import { Injectable } from '@angular/core'
import { EMPTY, Observable, of } from 'rxjs'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { FilterType } from 'src/app/models/filterType-enum'
import { CharacterService } from './character.service'

@Injectable({
  providedIn: 'root'
})
export class CharacterMockService implements CharacterService {
  private characters$: Observable<Character[]> = of([
    {
      id: '1',
      name: 'Test character',
      uid: '1',
      health: 0,
      mana: 0,
      stamina: 0,
      physicalPower: 0,
      magicalPower: 0,
      physicalDefense: 0,
      magicalDefense: 0,
      speed: 0,
      luck: 0,
      charisma: 0
    }
  ])
  private characterForms$: Observable<CharacterForm[]> = of([
    {
      id: '1',
      name: 'Test character',
      uid: '1',
      roleId: '1',
      roleName: 'Test Role',
      speciesId: '1',
      speciesName: 'Test Species'
    }
  ])
  myCharacters$: Observable<Character[]> = EMPTY
  myCharacterForms$: Observable<CharacterForm[]> = EMPTY

  getModels(): Observable<Character[]> {
    return this.characters$
  }
  getModelForms(): Observable<CharacterForm[]> {
    return this.characterForms$
  }
  getModel(id: string): Observable<Character> {
    return this.getCharacter(id)
  }
  getModelForm(id: string): Observable<CharacterForm> {
    return this.getCharacterForm(id)
  }

  createModel(formModel: CharacterForm): Promise<void> {
    return
  }
  private getCharacter(id: string): Observable<Character> {
    return of({
      id,
      name: 'Test character',
      uid: '1',
      health: 0,
      mana: 0,
      stamina: 0,
      physicalPower: 0,
      magicalPower: 0,
      physicalDefense: 0,
      magicalDefense: 0,
      speed: 0,
      luck: 0,
      charisma: 0
    })
  }
  private getCharacterForm(id: string): Observable<CharacterForm> {
    return of({
      id,
      name: 'Test character form',
      speciesId: '1',
      roleId: '1'
    })
  }
  async deleteModel(id: string) {
    return
  }
  async updateModel(model: CharacterForm) {
    return
  }
  filterCharacters(filterType: FilterType, character: CharacterForm) {
    return this.characterForms$
  }
}
