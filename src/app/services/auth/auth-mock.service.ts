import { Injectable } from '@angular/core'
import { EMPTY, Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { User } from 'src/app/models/user.model'
import { AuthService } from './auth.service'

@Injectable({
  providedIn: 'root'
})
export class AuthMockService implements AuthService {
  constructor() {}

  private user: User = {
    uid: '1',
    email: 'test@test.com',
    activated: true
  }

  user$: Observable<User> = of({
    uid: '1',
    email: 'test@test.com',
    activated: true
  })
  loggedIn$: Observable<boolean> = this.user$.pipe(map((user) => !!user))
  isAdmin$: Observable<boolean> = this.user$.pipe(map((user) => user.isAdmin))
  googleSignin(): Promise<void> {
    this.user$ = of(this.user)
    return
  }

  signOut(): Promise<void> {
    this.user$ = EMPTY
    return
  }
}
