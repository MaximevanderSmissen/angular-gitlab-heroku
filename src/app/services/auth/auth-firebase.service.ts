import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore'
import firebase from 'firebase/app'
import { User } from 'src/app/models/user.model'
import { AuthService } from './auth.service'
import { first, map, switchMap } from 'rxjs/operators'
import { EMPTY, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AuthFirebaseService implements AuthService {
  constructor(private afAuth: AngularFireAuth, private afs: AngularFirestore) {}

  user$: Observable<User> = this.afAuth.authState.pipe(
    switchMap((user) => {
      if (user) {
        return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
      } else {
        return EMPTY
      }
    })
  )
  loggedIn$: Observable<boolean> = this.afAuth.authState.pipe(map((user) => !!user))
  isAdmin$: Observable<boolean> = this.user$.pipe(map((user) => user.isAdmin))

  async googleSignin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    const credential = await this.afAuth.signInWithPopup(provider)
    return this.updateUser(credential.user)
  }

  private async updateUser(user: firebase.User) {
    const userRef: AngularFirestoreDocument<User> = this.afs.doc(`users/${user.uid}`)
    const currentUser: User = await this.user$.pipe(first()).toPromise()

    const data: User = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      isAdmin: currentUser ? currentUser.isAdmin : false,
      activated: currentUser ? currentUser.activated : true
    }

    return userRef.set(data, { merge: true })
  }

  async signOut() {
    await this.afAuth.signOut()
  }
}
