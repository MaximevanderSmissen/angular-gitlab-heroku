import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { User } from 'src/app/models/user.model'

@Injectable({
  providedIn: 'root'
})
export abstract class AuthService {
  abstract user$: Observable<User>
  abstract loggedIn$: Observable<boolean>
  abstract isAdmin$: Observable<boolean>
  abstract googleSignin(): Promise<void>
  abstract signOut(): Promise<void>
}
