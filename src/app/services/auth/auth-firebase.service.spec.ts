import { TestBed } from '@angular/core/testing'
import { AngularFireModule } from '@angular/fire'
import { RouterTestingModule } from '@angular/router/testing'
import { environment } from 'src/environments/environment'

import { AuthFirebaseService } from './auth-firebase.service'

describe('AuthFirebaseService', () => {
  let service: AuthFirebaseService

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireModule.initializeApp(environment.firebase)]
    })
    service = TestBed.inject(AuthFirebaseService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
