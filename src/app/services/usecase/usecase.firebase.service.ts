import { Injectable } from '@angular/core'
import { AngularFirestore } from '@angular/fire/firestore'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { UseCase } from 'src/app/models/usecase.model'
import { UsecaseService } from './usecase.service'

@Injectable({
  providedIn: 'root'
})
export class UsecaseFireBaseService implements UsecaseService {
  firestore: AngularFirestore

  constructor(firestore: AngularFirestore) {
    this.firestore = firestore
  }

  getUsecases(): Observable<UseCase[]> {
    return this.firestore
      .collection<UseCase>('usecases', (ref) => ref.orderBy('id'))
      .valueChanges()
  }

  getUsecase(id: string): Observable<UseCase> {
    return this.firestore
      .collection<UseCase>('usecases', (ref) => ref.where('id', '==', id))
      .valueChanges()
      .pipe(map((usecases) => (usecases.length === 1 ? usecases[0] : undefined)))
  }
}
