import { TestBed } from '@angular/core/testing'

import { MockUsecaseService } from './mock-usecase.service'

describe('MockUsecaseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}))

  it('should be created', () => {
    const service: MockUsecaseService = TestBed.inject(MockUsecaseService)
    expect(service).toBeTruthy()
  })
})
