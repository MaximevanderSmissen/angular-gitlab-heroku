import { TestBed } from '@angular/core/testing'
import { AngularFireModule } from '@angular/fire'
import { environment } from 'src/environments/environment'

import { UsecaseFireBaseService } from './usecase.firebase.service'

describe('UsecaseFireBaseService', () => {
  let service: UsecaseFireBaseService

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AngularFireModule.initializeApp(environment.firebase)]
    })
    service = TestBed.inject(UsecaseFireBaseService)
  })
  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
