import { Observable } from 'rxjs'
import { UseCase } from 'src/app/models/usecase.model'

export abstract class UsecaseService {
  abstract getUsecases(): Observable<UseCase[]>
  abstract getUsecase(id: string): Observable<UseCase>
}
