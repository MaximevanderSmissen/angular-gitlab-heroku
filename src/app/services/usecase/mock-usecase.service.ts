import { Injectable } from '@angular/core'
import { from, Observable } from 'rxjs'
import { UseCase } from 'src/app/models/usecase.model'
import { UsecaseService } from './usecase.service'

@Injectable({
  providedIn: 'root'
})
export class MockUsecaseService implements UsecaseService {
  private useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Log in',
      description: 'An existing user logs in using this.',
      scenario: [
        'User fills in their username and password and pushes the "login" button.',
        'The application validates the given data.',
        'Assuming the credentials are correct, the application redirects to the homepage.'
      ],
      actor: 'Any user.',
      precondition: 'Actor has an account.',
      postcondition:
        'The actor is logged in and gets a token, which is send with every next request as indication that the user is logged in.'
    }
  ]

  constructor() {}

  getUsecases(): Observable<UseCase[]> {
    return from([this.useCases])
  }

  getUsecase(id: string): Observable<UseCase> {
    this.useCases.forEach((useCase) => {
      if (useCase.id === id) {
        return from([useCase])
      }
    })

    return from([])
  }
}
