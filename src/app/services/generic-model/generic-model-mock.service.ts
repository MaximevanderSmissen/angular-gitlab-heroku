import { Injectable } from '@angular/core'
import { Observable, of } from 'rxjs'
import { map } from 'rxjs/operators'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModel, GenericModelForm } from 'src/app/models/genericModel.model'
import { CharacterMockService } from '../character/character-mock.service'
import { GenericModelService } from './generic-model.service'

@Injectable({
  providedIn: 'root'
})
export class GenericModelMockService implements GenericModelService {
  constructor(private characterService: CharacterMockService) {}

  createModel(formModel: GenericModelForm, type: GenericModelType): Promise<void> {
    return
  }
  deleteModel(id: string, type: GenericModelType): Promise<void> {
    return
  }
  updateModel(model: GenericModelForm, type: GenericModelType): Promise<void> {
    return
  }
  getModels(type: GenericModelType): Observable<GenericModel[]> {
    switch (type) {
      case GenericModelType.role:
        return of([
          {
            id: '1',
            name: 'Test Role',
            uid: '1',
            health: 0,
            mana: 0,
            stamina: 0,
            physicalPower: 0,
            magicalPower: 0,
            physicalDefense: 0,
            magicalDefense: 0,
            speed: 0,
            luck: 0,
            charisma: 0
          }
        ])
      case GenericModelType.species:
        return of([
          {
            id: '1',
            name: 'Test Species',
            health: 0,
            mana: 0,
            stamina: 0,
            physicalPower: 0,
            magicalPower: 0,
            physicalDefense: 0,
            magicalDefense: 0,
            speed: 0,
            luck: 0,
            charisma: 0
          }
        ])
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  getModelForms(type: GenericModelType): Observable<GenericModelForm[]> {
    return this.getModels(type)
  }
  getModel(id: string, type: GenericModelType): Observable<GenericModel | Character> {
    switch (type) {
      case GenericModelType.character:
        return this.characterService.getModel(id).pipe(
          map((character) => {
            delete character.uid
            return character
          })
        )
      case GenericModelType.role:
        return of({
          id: '1',
          name: 'Test Role',
          uid: '1',
          health: 0,
          mana: 0,
          stamina: 0,
          physicalPower: 0,
          magicalPower: 0,
          physicalDefense: 0,
          magicalDefense: 0,
          speed: 0,
          luck: 0,
          charisma: 0
        })
      case GenericModelType.species:
        return of({
          id: '1',
          name: 'Test Species',
          health: 0,
          mana: 0,
          stamina: 0,
          physicalPower: 0,
          magicalPower: 0,
          physicalDefense: 0,
          magicalDefense: 0,
          speed: 0,
          luck: 0,
          charisma: 0
        })
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  getModelForm(id: string, type: GenericModelType): Observable<GenericModelForm | CharacterForm> {
    switch (type) {
      case GenericModelType.character:
        return this.characterService.getModelForm(id).pipe(
          map((character) => {
            delete character.uid
            return character
          })
        )
      case GenericModelType.role:
        return of({
          id: '1',
          name: 'Test Role'
        })
      case GenericModelType.species:
        return of({
          id: '1',
          name: 'Test Species'
        })
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
}
