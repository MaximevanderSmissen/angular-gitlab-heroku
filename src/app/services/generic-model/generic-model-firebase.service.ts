import { Injectable } from '@angular/core'
import { AngularFirestore } from '@angular/fire/firestore'
import { Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModel, GenericModelForm } from '../../models/genericModel.model'
import { CharacterService } from '../character/character.service'
import { GenericModelService } from './generic-model.service'

@Injectable({
  providedIn: 'root'
})
export class GenericModelFirebaseService implements GenericModelService {
  constructor(private afs: AngularFirestore, private characterService: CharacterService) {}

  async createModel({ id, ...formModel }: GenericModelForm, type: GenericModelType): Promise<void> {
    switch (type) {
      case GenericModelType.role:
      case GenericModelType.species:
        await this.afs.collection(type).add(formModel)
        break
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  async deleteModel(id: string, type: GenericModelType): Promise<void> {
    switch (type) {
      case GenericModelType.role:
      case GenericModelType.species:
        await this.afs.doc(`${type}/${id}`).delete()
        break
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  async updateModel(model: GenericModelForm, type: GenericModelType): Promise<void> {
    switch (type) {
      case GenericModelType.role:
      case GenericModelType.species:
        await this.afs.doc<GenericModelForm>(`${type}/${model.id}`).set(model)
        break
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  getModels(type: GenericModelType): Observable<GenericModel[]> {
    switch (type) {
      case GenericModelType.role:
      case GenericModelType.species:
        return this.afs
          .collection<GenericModel>(type, (ref) => ref.orderBy('name'))
          .valueChanges({ idField: 'id' })
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  getModelForms(type: GenericModelType): Observable<GenericModelForm[]> {
    return this.getModels(type)
  }
  getModel(id: string, type: GenericModelType): Observable<GenericModel | Character> {
    switch (type) {
      case GenericModelType.character:
        return this.characterService.getModel(id).pipe(
          map((character) => {
            delete character.uid
            return character
          })
        )
      case GenericModelType.role:
      case GenericModelType.species:
        return this.afs.doc<GenericModel>(`${type}/${id}`).valueChanges({ idField: 'id' })
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
  getModelForm(id: string, type: GenericModelType): Observable<GenericModelForm | CharacterForm> {
    switch (type) {
      case GenericModelType.character:
        return this.characterService.getModelForm(id).pipe(
          map((character) => {
            delete character.uid
            return character
          })
        )
      case GenericModelType.role:
      case GenericModelType.species:
        return this.afs.doc<GenericModelForm>(`${type}/${id}`).valueChanges({ idField: 'id' })
      default:
        throw new Error('Unsupported type given to Generic Model Service')
    }
  }
}
