import { TestBed } from '@angular/core/testing'

import { GenericModelMockService } from './generic-model-mock.service'

describe('GenericModelMockService', () => {
  let service: GenericModelMockService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(GenericModelMockService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
