import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { Character, CharacterForm } from 'src/app/models/character.model'
import { GenericModelType } from 'src/app/models/generic-model-type-enum'
import { GenericModel, GenericModelForm } from '../../models/genericModel.model'

@Injectable({
  providedIn: 'root'
})
export abstract class GenericModelService {
  abstract getModels(type: GenericModelType): Observable<GenericModel[]>
  abstract getModelForms(type: GenericModelType): Observable<GenericModelForm[]>
  abstract getModel(id: string, type: GenericModelType): Observable<GenericModel | Character>
  abstract getModelForm(id: string, type: GenericModelType): Observable<GenericModelForm | CharacterForm>
  abstract createModel(formModel: GenericModelForm, type: GenericModelType): Promise<void>
  abstract deleteModel(id: string, type: GenericModelType): Promise<void>
  abstract updateModel(model: GenericModelForm, type: GenericModelType): Promise<void>
}
