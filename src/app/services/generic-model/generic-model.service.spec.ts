import { TestBed } from '@angular/core/testing'

import { GenericModelService } from './generic-model.service'

describe('CompareService', () => {
  let service: GenericModelService

  beforeEach(() => {
    TestBed.configureTestingModule({})
    service = TestBed.inject(GenericModelService)
  })

  it('should be created', () => {
    expect(service).toBeTruthy()
  })
})
