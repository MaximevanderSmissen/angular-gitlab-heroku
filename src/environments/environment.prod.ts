const packagejson = require('../../package.json')

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyBjw4g-pjLCwLtLQ8wz42_BTT4R-pBE-gw',
    authDomain: 'valemnia-blood-moon.firebaseapp.com',
    databaseURL: 'https://valemnia-blood-moon.firebaseio.com',
    projectId: 'valemnia-blood-moon',
    storageBucket: 'valemnia-blood-moon.appspot.com',
    messagingSenderId: '576211541959',
    appId: '1:576211541959:web:8522590309e249000243e2',
    measurementId: 'G-QP7QL3QYH6'
  },

  // Fill in your own online server API url here
  apiUrl: 'https://client-side-web-programming.herokuapp.com/',

  version: packagejson.version
}
