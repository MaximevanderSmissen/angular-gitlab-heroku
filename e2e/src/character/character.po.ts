import { by, element, ElementArrayFinder, ElementFinder } from 'protractor'
import { CommonPageObject } from '../common.po'

export class CharactersPage extends CommonPageObject {
  get pageTitle() {
    return element(by.tagName('h1')) as ElementFinder
  }
}
