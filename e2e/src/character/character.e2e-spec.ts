import { CharactersPage } from './character.po'
import { browser, logging } from 'protractor'

describe('About page', () => {
  let page: CharactersPage

  beforeEach(() => {
    page = new CharactersPage()
    page.navigateTo('/character')
  })

  /**
   *
   */
  it('should be at /character route after initialisation', () => {
    browser.waitForAngularEnabled(false)
    expect(browser.getCurrentUrl()).toContain('/character')
  })

  it('should display the correct page title text.', () => {
    browser.waitForAngularEnabled(false)
    const pageTitle = page.pageTitle

    expect(pageTitle.getText()).toContain('Characters')
  })

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER)
    expect(logs).not.toContain(
      jasmine.objectContaining({
        level: logging.Level.SEVERE
      } as logging.Entry)
    )
  })
})
